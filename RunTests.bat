@echo off
cd %~dp0tests

:AskUnitTests
echo Would you like to run unit tests?(Y/N)
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="y" goto yes 
If /I "%INPUT%"=="n" goto AskFuncTests
echo Incorrect input & goto AskUnitTests
:yes
start phpunit6 UnitTests.php
goto AskFuncTests
:AskFuncTests
echo Would you like to run unit tests?(Y/N)
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="y" goto yes 
If /I "%INPUT%"=="n" goto Exit
echo Incorrect input & goto AskFuncTests
:yes
start phpunit6 FunctionalTests.php
:EXIT
exit