<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db;

    public  static $DSN = "mysql:host=localhost;dbname=bookdb;charset=utf8mb4";
    private static $USER = 'root';
    private static $PASSWORD = '';



    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db)
        { $this->db = $db; }
		else
		{
            file_put_contents('php://stderr', print_r("no PDO created yet, creating one 4 u", TRUE));
            $this->db = new PDO(self::$DSN,self::$USER, self::$PASSWORD);
            $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();
        $qry = $this->db->prepare("SELECT * FROM `books`");
        try
        {
            $qry->execute();
            while($row = $qry->fetch(PDO::FETCH_ASSOC))
            {
                $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
            }
        }
        catch (PDOException $ex)
        {
            file_put_contents('php://stderr', print_r("FUNC:getBookList() -> " . $ex->getMessage(), TRUE));
        }
        return $booklist;
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        if(!is_numeric($id)) //dont allow non-numerical id's
        {
            $id = -1;
        }
        $book = NULL;
        $qry = $this->db->prepare('SELECT * FROM books WHERE id =:id');
        try
        {
            $qry->execute(array(':id' => $id));
            if($data = $qry->fetch(PDO::FETCH_ASSOC)) //if id connected with existing data
            {
                $book = new Book($data['title'], $data['author'], $data['description'], $data['id']);
            }
        }
        catch (PDOException $ex)
        {
            //print to server error log
            file_put_contents('php://stderr', print_r("FUNC:getBookById('$book') -> " . $ex->getMessage(), TRUE));
        }
    return $book;
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     * @throws DomainException
     */
    public function addBook($book)
    {
        if(!$this->validBook($book))
        {
            throw new DomainException("Invalid book! title or author is empty");
        }
        else
        {
            $qry = $this->db->prepare(
                "INSERT INTO `books` (`title`, `author`, `description`)
                VALUES (:title, :author, :description);"
            );
            try
            {
                $qry->execute(array(
                    ':title'        =>$book->title,
                    ':author'       =>$book->author,
                    ':description'  =>$book->description
                ));
                $book->id = $this->db->lastInsertId();
            }
            catch (PDOException $ex)
            {
                //print to server error log
                file_put_contents('php://stderr', print_r("FUNC:addBook('$book') -> " . $ex->getMessage(), TRUE));
            }
        }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @throws PDOException
     */
    public function modifyBook($book)
    {
        if(!$this->validBook($book))
        {
            throw new DomainException("Invalid book! title or author is empty");
        }
        else
        {
            $qry = $this->db->prepare(
                "UPDATE `books`
                SET `title` = :title, `author` = :author, `description` = :description
                WHERE `id` = :id"
            );
            try
            {
                $qry->execute(array(
                    ':title'        =>$book->title,
                    ':author'       =>$book->author,
                    ':description'  =>$book->description,
                    ':id'           =>$book->id
                ));
            }
            catch (PDOException $ex)
            {
                //print to server error log
                file_put_contents('php://stderr', print_r("FUNC:modifyBook('$book') -> " . $ex->getMessage(), TRUE));
            }
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @throws PDOException
     */
    public function deleteBook($id)
    {
        $qry = $this->db->prepare(
            "DELETE FROM `books`
            WHERE `id` = :id"
        );
        try
        {
            $qry->execute(array(':id' => $id));
        }
        catch (PDOException $ex)
        {
            //print to server error log
            file_put_contents('php://stderr', print_r("FUNC:modifyBook('$book') -> " . $ex->getMessage(), TRUE));
        }

    }

    /** Checks book's validity
     * @param $book Book The book data to be kept.
     * @return bool Validity of the book - is the title and author set?
     */
    protected function validBook($book)
    {
        return (!empty($book->title) && !empty($book->author));
    }

}

?>
