@echo off

rmdir "C:\xampp\htdocs\IMT2571\assignment1\LAST_VERSION" /S /Q
mkdir "C:\xampp\htdocs\IMT2571\assignment1\LAST_VERSION"

xcopy /Y "C:\xampp\htdocs\IMT2571\assignment1" "C:\xampp\htdocs\IMT2571\assignment1\LAST_VERSION\"

rmdir "C:\xampp\htdocs\IMT2571\assignment1" /S /Q
mkdir "C:\xampp\htdocs\IMT2571\assignment1"

xcopy %~dp0src "C:\xampp\htdocs\IMT2571\assignment1\" /S

cd %~dp0tests

:AskUnitTests
echo Would you like to run unit tests?(Y/N)
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="y" goto yes 
If /I "%INPUT%"=="n" goto AskFuncTests
echo Incorrect input & goto AskUnitTests
:yes
start phpunit6.cmd UnitTests.php
goto AskFuncTests
:AskFuncTests
echo Would you like to run unit tests?(Y/N)
set INPUT=
set /P INPUT=Type input: %=%
If /I "%INPUT%"=="y" goto yes 
If /I "%INPUT%"=="n" goto RUN
echo Incorrect input & goto AskFuncTests
:yes
start phpunit6.cmd FunctionalTests.php
goto RUN
:RUN
start "TESTING" http://localhost/IMT2571/assignment1/index.php